package com.company;

public class setUp {
    private String status;
    private int volume = 0;
    private int brightness = 0;
    private String cable = "VGA";
    private String [] cableList = {"VGA", "DVI", "HDMI", "DisplayPort"};

    public String turnOff(){
        return status = "Off";
    }

    public String turnOn(){
        return status = "On";
    }

    public String freeze(){
        return status = "Freeze";
    }

    public int volumeUp(){
        return volume++;
    }

    public int volumeDown(){
        return volume--;
    }

    public void setVolume(int volume){
        this.volume = volume;
    }

    public int brightnessUp(){
        return brightness++;
    }

    public int brightnessDown(){
        return brightness--;
    }

    public void setBrightness(int brightness){
        this.brightness = brightness;
    }

    public void setCable(String cable){
        this.cable = cable;
    }

    public void cableUp(){
        for(int i = 0; i < cableList.length; i++){
            if(cable.equals(cableList[i])){
                if(cable.equals(cableList[3])){
                    cable = cableList[3]; break;
                } else {
                    cable = cableList[i + 1]; break;
                }
            }
        }
    }

    public void cableDown(){
        for(int i = 0; i < cableList.length; i++){
            if(cable.equals(cableList[i])){
                if(cable.equals(cableList[0])){
                    cable = cableList[0]; break;
                } else { cable = cableList[i-1]; break;
                }
            }
        }
    }

    public void displayStatus(){
        System.out.println("-------------------------------");
        System.out.println("\t\t  LCD STATUS");
        System.out.println("-------------------------------");
        System.out.println("LCD Status     : " + status);
        System.out.println("LCD Volume     : " + volume);
        System.out.println("LCD Brightness : " + brightness);
        System.out.println("LCD Cable      : " + cable);
    }
}
